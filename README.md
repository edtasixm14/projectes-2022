# Projectes 2022

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2021-2022

#### M14 Projecte

ASIX M14 Projectes Escola del treball de Barcelona. Curs 2021-2022.


1) Joan Almirall i Kevin Valencia: **Big Data / Machine learning**

   GIT: https://gitlab.com/big-data-and-machine-learning-kevin-joan/asix-2022/-/tree/master

   Trello: https://trello.com/b/5uoQ0YfZ/big-data-and-machine-learning-kevin-joan


2) Albert Pujades: **AI i Machine learning**

   GIT: https://gitlab.com/albertpg2002/projecteasix

   TRELLO: https://trello.com/b/Oer0wutr/treball-final-de-grau-albert-pujadas-g%C3%B3mez


3) Cristian Condolo i Aaron Andal: **Seguretat**

   GIT: https://github.com/KeshiKiD03/asixproject2k22

   Trello: https://trello.com/b/kQt8ZIrK/progreso


4) Marc Fornés i Ruben Rodríguez: **virtual machine & deployments**

   GIT: https://gitlab.com/rubeeenrg/projecte_asix2.git

   Trello: https://trello.com/b/Gn5vqlTJ/planning-del-projecte


5) Albert Blanco i Jamison Quelal: **LPIC-3 303 Security**

   GIT: https://github.com/isx24432143/Projecte-ASIX


6) Hemiadies Castillo: **???**

   GIT:



